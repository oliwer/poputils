#!/bin/bash

#
# Connexion et envoi de commandes rapide vers les VM TAT-1
#

set -eu

usage()
{
  cat << EOF
usage: ash <hostname> [ep] [commandes...]"

ash permet de se connecter ou d'exécuter des commandes sur une VM TAT-1
sans avoir a se connecter sur la MTL au préalable.

Si vous ne précisez pas l'EP de la machine à la quelle se connecter, ash
tente de la deviner automatiquement en fonction du numéro de VM.

Contrairement à ssh, ash ne lit pas sur stdin. Vous ne pourrez donc pas
vous en servir pour transférer des fichiers.

Comme avec ssh, vous pouvez passer des commandes en arguments, ou bien
une des commandes prédéfinies :

  reconfig        lance reconfig.sh avec sudo
  iptables        lance iptables -L avec sudo

EOF
  exit 1
}

# On utilise pas poplib.sh afin de rester portable.
ep2mtl()
{
  local EP=$1

  case ${EP} in
  D|E)
    echo altais ;;
  A|I|S|P|R|Y)
    echo alcyone ;;
  *)
    echo >&2 "erreur: EP invalide '$EP'"
    usage
  esac
}

# On a juste besoin de distingué l'essai de la prod.
guessEP()
{
  local vm=$1

  case ${vm:7:2} in
    01|10|11|02|20|12) echo E ;;
    *) echo P ;;
  esac
}

# Main
[ $# -ge 1 ] || usage
vm=$1
if [ $# -ge 2 ] && [ ${#2} -eq 1 ]; then
  EP=${2^^}
  shift 2
else
  EP=$(guessEP $vm)
  shift
fi

case "${1:-}" in
reconfig)
  cmd="sudo /Agora/build/config/reconfig.sh" ;;
iptables)
  cmd="sudo iptables -L" ;;
*)
  cmd="$@"
esac

mtl=$(ep2mtl $EP)
mtlcmd="su -c \"ssh -t $vm $cmd\" admsys"

exec ssh -t root@$mtl "$mtlcmd"
