#!/usr/bin/env perl

#
# findep.pl - Cherche une VM dans le FWAP et retourne son EP
#

use strict;
use warnings;

my ($ep, $vm);

$vm = $ARGV[0] or die "usage: $0 <VM>\n";

open my $fh, '<', '/Agora/fic/FWAP.xml' or
  die "Impossible d'ouvrir le FWAP: $!\n";

while (<$fh>) {
  if (/Environnement EP="([A-Y])"/) {
    $ep = $1;
    next;
  }
  if (/MachineVirtuelle SERVERNAME="$vm"/) {
    print "$ep\n";
    exit 0;
  }
}

die "VM introuvable dans le FWAP : $vm\n";
