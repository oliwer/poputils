poputils
========

Une collection de scripts pour accélérer la création de VM TAT-1.

Chaque script a son message d'aide détaillé visible en le lançant sans argument.

fwap
----

    fwap get EP RDS

Télécharge un modèle FWAP sur votre bureau et édite automatiquement certains
champs en fonction des informations disponibles.

Il est conseillé de définir la variable d'environnement `FWAP_EDITOR` qui indique
le chemin vers votre éditeur de texte préféré. Celui-ci sera automatiquement lancé
après le téléchargement d'un modèle.

ash
---

    ash <hostname> [ep]

Un script pour se connecter rapidement à une VM, sans avoir a passer par la MTL.
Le nom de la MTL a utiliser est deviné en fonction du numéro de la VM. Si celui-ci
n'est pas conforme à la norme, préciser l'EP après le nom de la VM.


**Les scripts suivants sont a lancer depuis la MTL en root**

vmrapa
------

    vmrapa <hostname>

Installation de répartiteur Apache. A lancé après le premier démarrage de la VM.

Actions: wxvc02, wxvc03, mot de passe root.

vmsa
----

    vmsa <hostname>

Installation d'un VM JBoss. A lancé après le premier démarrage de la VM. Le RAPA
doit être prêt (vérifié par le script).

Actions: wxvc02, wxvc03, wxvc07, y20ctx, y20ct4, rootPass

vmpg
----

    vmpg <hostname>

Installation d'une VM PostgreSQL. A lancé après le premier démarrage de la VM.
Ce script initialisera une instance si la VM n'est pas en essai ou pré-essai.

Actions: wxvc02, wxvc03, wxvc08, copyDBFiles, y20ctx, y20ct4, rootPass, initInstance, setPasswords

vmora
-----

Pour les VM Oracle. Pas encore implémenté.

 
workflow conseillé
==================

1. demandes IP + NFS
2. fwap get $EP $RDS
3. édition du fwap et copie sur la MTL
4. création et lancement de la VM dans VMWare
5. lancement du script adapté
6. Installer JE + EON

En cas d'erreur
---------------

Les messages d'erreurs sont visibles sur stderr, et un fichier log plus détaillé
est présent sous /tmp/nom_du_script.log

Sur la MTL, les scripts sont installés dans /root/bin.


