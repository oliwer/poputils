#!/bin/bash

#
# vmsa - Configuration automatique d'une VM TAT-1 SA
#

set -euo pipefail
source "$(dirname $0)/poplib.sh"

usage()
{
  echo "usage: vmsa <hostname> [step]"
  echo "exemple: vmsa a82slna03"
  echo "Vous pouvez préciser le nom de l'étape a éxécuter parmi :"
  echo "  wxvc02"
  echo "  wxvc03"
  echo "  wxvc07"
  echo "  y20ctx"
  echo "  y20ct4"
  echo "  rootPass"
  echo
  echo "Toutes les étapes sont exécutées par défaut."
  echo
  exit $1
}

setup()
{
  loginit "/tmp/vmsa.log"
  vm="$1"
  validVM "$vm"
  EP=$(findep.pl $vm)
  validEP "$EP"
  RDS=$(uc ${vm:3:4})
  validRDS "$RDS" S
  info "EP=$EP  RDS=$RDS  VM=$vm"
  trap error_handler ERR
  lookupvm "$vm"
}

all_steps()
{
  wxvc02 "$EP" "$RDS"
  wxvc03 "$EP" "$RDS" "$vm"
  wxvc07 "$EP" "$RDS" "$vm"
  y20ctx "$EP" "$RDS"
  y20ct4 "$EP" "$RDS" "$vm"
  rootPass "$vm"
  info "Instllation de $vm terminée."
  echo
}

run_step()
{
  local step="$1"
  
  case "$step" in
    wxvc02|y20ctx) "$step" "$EP" "$RDS" ;;
    wxvc03|wxvc07|y20ct4) "$step" "$EP" "$RDS" "$vm" ;;
    rootPass) "$step" "$vm" ;;
    *) die "commande invalide: '$step'"
  esac
}

# Variables globales
EP=
RDS=
vm=

# Main
require_root
on_mtl

case $# in
  0)
    usage 0 ;;
  1)
    setup "$1"
    all_steps ;;
  2)
    setup "$1"
    run_step "$2" ;;
  *)
    usage 1 ;;
esac
