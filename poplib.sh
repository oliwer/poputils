#
# poplib.sh - routines partagées
#

# Nom du fichier log, défini par le programme appelant
logf=/dev/null

# Doit être appellé avant d'utiliser log(), info() et die()
loginit()
{
  logf="$1"
  date >"$logf"
}

log()
{
  echo "$*" >> "$logf"
}

info()
{
  echo "$*" | tee -a "$logf"
}

die()
{
  echo " *** erreur *** $*" | tee -a "$logf"
  exit 1
}

error_handler()
{
  die "Une erreur est survenue. Veuillez consulter le fichier log : $logf"
}

uc()
{
  echo "$*" | tr '[a-z]' '[A-Z]'
}

lc()
{
  echo "$*" | tr '[A-Z]' '[a-z]'
}

require_root()
{
  if [ $(id -u) -ne 0 ]; then
    echo >&2 "Vous devez lancer ce programme en root."
    exit 1
  fi
}

on_mtl()
{
  case $(hostname -s) in
    a82amtl*)
      : ;;
    *)
      echo >&2 "Ce programme doit être lancé depuis une MTL"
      exit 1 ;;
  esac
}

pingvm()
{
  local vm="$1"

  # Cette fonction ne supporte que GNU ping. MobaXterm utilise
  # le ping de Windows qui ne retrourne pas de code d'erreur.
  [ "$(uname -o)" != Cygwin ] || return 0

  ping -c 1 -w 1 "$vm" >/dev/null || die "La VM '$vm' n'est pas joignable"
}

lookupvm()
{
  local vm="$1" ip

  ip=$(dig +short $vm.agora.msanet)
  [ -n "$ip" ] || die "DNS: pas d'IP trouvée pour '$vm.agora.msanet'"

  # Si l'IP est sur l'intranet, on peut essayer de la pinger
  if [ ${ip:0:7} = 170.217 ]; then
    pingvm "$vm"
  fi
}

validEP()
{
  local EP="$1"

  case "$EP" in
    A|D|E|I|S|P|Y) return 0 ;;
    *) die "EP invalide : $EP" ;;
  esac
}

validRDS()
{
  local RDS="$1" TYPE=${2:-}

  [ ${#RDS} -eq 4 ] || die "RDS invalide : $RDS"
  [ -n "$TYPE" ] || return 0
  [ ${RDS:0:1} = "$TYPE" ] || die "RDS de type ${TYPE}XXX attendu"
}

validVM()
{
  local vm="$1"
  [[ $vm =~ ^[a-z][0-9][0-9][a-z][a-z][a-z][a-z0-9][0-9][0-9]$ ]] \
    || die "nom de VM invalide : '$vm'"
}

# Détermine la MTL a utiliser en fonction de l'EP
ep2mtl()
{
  local EP="$1"
  
  case "$EP" in
    D|E) echo a82amtl01 ;;
    A|I|S|P|Y) echo a82amtl02 ;;
    *) die "EP invalide : $EP"
  esac
}

# Détermine le numéro de la VM en fonction de l'EP
ep2num()
{
  local EP="$1" n

  case "$EP" in
    D) n=01 ;;
    E) n=02 ;;
    I) n=03 ;;
    S) n=04 ;;
    P) n=05 ;;
    R|A|Y) n=06 ;;
    *) die "EP invalide : $EP"
  esac

  echo $n
}

# Mot de passe aléatoire de 10 caractères par défaut
gen_password()
{
  local tab="abcdefghijklnmoprstuvwxyz0123456789"
  local len=${1:-10} p="" i

  for i in $(seq 1 $len); do
    p+=${tab:RANDOM%${#tab}:1}
  done

  echo $p
}

wxvc02()
{
  local EP="$1" RDS="$2"
  
  info "Lancement de WXVC02..."
  # On ignore les erreurs pour ce script qui est buggé...
  /N${EP}00P/U${EP}SYS/bin/WXVC02.sh "$EP" "$RDS" 2>&1 >>"$logf" \
    || true
}

wxvc03()
{
  local EP="$1" RDS="$2" vm="$3"
  
  info "Lancement de WXVC03..."
  /N${EP}00P/U${EP}SYS/bin/WXVC03.sh "$EP" "$RDS" "$vm" 2>&1 >>"$logf"
}

wxvc07()
{
  local EP="$1" RDS="$2" vm="$3"
  
  info "Vérification du répartiteur Apache..."
  eval $(/N${EP}00P/U${EP}SYS/bin/WXVC01.sh pvm $EP $RDS $vm | grep rds_apache)
  [ -n "$rds_apache" ] || die "Pas de rds_apache défini pour $vm"
  local rapa=$(/N${EP}00P/U${EP}SYS/bin/WXVC01.sh lvm $EP $rds_apache | head -n1)
  [ -n "$rapa" ] || die "Pas de VM trouvée pour le RDS $rds_apache"
  pingvm "$rapa"
  
  info "Lancement de WXVC07..."
  local user=admamtl$(lc $EP)
  su -c "/N${EP}00P/U${EP}SYS/bin/WXVC07.sh $EP $RDS" $user 2>&1 >>"$logf"
}

wxvc08()
{
  local EP="$1" RDS="$2" vm="$3"

  info "Lancement de WXVC08..."
  /N${EP}00P/U${EP}SYS/bin/WXVC08.sh "$EP" "$RDS" "$vm" 2>&1 >>"$logf"
}

y20ctx()
{
  local EP="$1" RDS="$2"
  
  [ -z "${SKIP_Y20:-}" ] || return 0
  info "Lancement de Y20CTX..."
  su -c "/N${EP}00P/U${EP}AGL/bin/Y20CTX.sh T $EP $RDS" agora 2>&1 >>"$logf"
}

y20ct4()
{
  local EP="$1" RDS="$2" vm="$3"
  
  [ -z "${SKIP_Y20:-}" ] || return 0
  info "Lancement de Y20CT4..."
  su -c "/N${EP}00P/U${EP}AGL/bin/Y20CT4.sh T $EP $RDS $vm" agora 2>&1 >>"$logf"
}

rootPass()
{
  local vm="$1" p=$(gen_password)
  
  info "Modification du mot de passe root..."
  echo "=> entrez le mot de passe actuel:"
  ( ssh -t -i ~admsys/.ssh/id_rsa admsys@$vm su -c "\"echo root:$p | chpasswd\"" root ) 2>&1 >>"$logf"
  echo
  echo "  Nouveau mot de passe : $p"
  echo
  echo "*** Notez-le maintenant ou perdez-le à tout jamais ! ***"
  echo
}
